import React from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import "./DetailUser.scss";
class DetailUser extends React.Component {
  state = {
    users: {},
  };
  async componentDidMount() {
    if (this.props.match && this.props.match.params) {
      let id = this.props.match.params.id;
      try {
        let res = await axios.get(`https://reqres.in/api/users/${id}`);
        console.log("asdasdasd0", res.data.data);
        this.setState({
          users: res && res.data && res.data.data ? res.data.data : {},
        });
      } catch {}
    }
  }
  handleBackButton = () => {
    this.props.history.push(`/user`);
  };
  render() {
    let { users } = this.state;
    console.log("users", users);
    let isEmptyObj = Object.keys(users).length === 0;
    return (
      <>
        <div>Detail {this.props.match.params.id}</div>
        {isEmptyObj === false && (
          <>
            <div>
              Tên: {users.first_name} {users.last_name}
            </div>
            <div>Email: {users.email}</div>
            <div>
              <img src={users.avatar}></img>
            </div>
            <div>
              <button
                type="button"
                onClick={() => this.handleBackButton()}
                className="cursor-detail"
              >
                Back
              </button>
            </div>
          </>
        )}
      </>
    );
  }
}
export default withRouter(DetailUser);
