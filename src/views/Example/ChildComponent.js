import React from "react";
import "./Home.scss";
class ChildComponent extends React.Component {
  /**
   *
   *  JSX return 1 block
   * Eg. JSX: const element = <h1>Hello world!</h1>;
   * use <React.Fragment></React.Fragment> or <></> if want return >= 2 block
   *
   */

  state = {
    showJobs: false,
  };
  handleShowHide = (state) => {
    this.setState({
      showJobs: !this.state.showJobs,
    });
  };
  handleOnClickDelete = (job) => {
    this.props.deleteAJob(job);
  };
  render() {
    let { arrJobs } = this.props;
    let { showJobs } = this.state;
    return (
      <>
        {showJobs === false ? (
          <div>
            <button className="btn-show" onClick={() => this.handleShowHide()}>
              Show
            </button>
          </div>
        ) : (
          <>
            <div className="job-lists">
              {arrJobs.map((item, index) => {
                return (
                  <div key={item.id}>
                    {item.title} - {item.salary} &nbsp;{" "}
                    <span onClick={() => this.handleOnClickDelete(item)}>
                      <button>x</button>
                    </span>
                  </div>
                );
              })}
            </div>
            <div>
              <button onClick={() => this.handleShowHide()}>Hide</button>
            </div>
          </>
        )}
      </>
    );
  }
}

// const ChildComponent = (props) => {
//   let { arrJob } = props;
//   return (
//     <>
//       <div className="job-lists">
//         {arrJob.map((item, index) => {
//           if (item.salary >= 500) {
//             return (
//               <div key={item.id}>
//                 {item.title} - {item.salary} $
//               </div>
//             );
//           }
//         })}
//       </div>
//     </>
//   );
// };
export default ChildComponent;
