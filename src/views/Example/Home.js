import React from "react";
import { withRouter } from "react-router-dom";
import Color from "../HOC/Color";
import "./Home.scss";
import background from "../../assets/images/background.png";
import { connect } from "react-redux";
class Home extends React.Component {
  // componentDidMount() {
  //   setTimeout(() => {
  //     this.props.history.push("/todo");
  //   }, 3000);
  // }
  hanleDeleteUser = (user) => {
    console.log("check delete", user);
    this.props.deleteUserRedux(user);
  };
  handleCreateUser = () => {
    this.props.addUserRedux();
  };
  render() {
    let listUsers = this.props.dataRedux;
    return (
      <>
        <div className="">
          <img src={background}></img>
        </div>
        <div className="background">Hello Home</div>

        <button className="background2" onClick={() => this.handleCreateUser()}>
          Add
        </button>
        <div className="background1">
          {listUsers &&
            listUsers.length > 0 &&
            listUsers.map((item, index) => {
              return (
                <div key={item.id} className="border">
                  {index + 1} {item.name} &nbsp;
                  <span
                    className="cursor"
                    onClick={() => this.hanleDeleteUser(item)}
                  >
                    x
                  </span>
                </div>
              );
            })}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return { dataRedux: state.users };
};
const mapDispathchToProps = (dispath) => {
  return {
    deleteUserRedux: (userDelete) =>
      dispath({
        type: "DELETE_USER",
        payload: userDelete,
      }),
    addUserRedux: () => dispath({ type: "CREATE_USER" }),
  };
};
export default connect(mapStateToProps, mapDispathchToProps)(Color(Home));
